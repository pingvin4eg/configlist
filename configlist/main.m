//
//  main.m
//  configlist
//
//  Created by Sergey Starukhin on 05.05.15.
//  Copyright (c) 2015 Sergey Starukhin. All rights reserved.
//

#import <Foundation/Foundation.h>

@import AppKit;

static NSString * const ConfiglistErrorDomain = @"com.star-s.configlist.error";

static NSArray * CmdLineArgumentsAsArray(int argc, const char * argv[])
{
    NSMutableArray *result = [NSMutableArray array];
    
    for (int i = 0; i < argc; i++) {
        [result addObject: [NSString stringWithCString: argv[i] encoding: NSUTF8StringEncoding]];
    }
    return result;
}

static NSArray * ListOfConfigs(NSDictionary *project)
{
    NSDictionary *objects = project[@"objects"];
    
    NSString *rootObjectKey = project[@"rootObject"];
    NSDictionary *rootObject = objects[rootObjectKey];
    
    NSString *buildConfigurationListKey = rootObject[@"buildConfigurationList"];
    NSDictionary *buildConfigurationList = objects[buildConfigurationListKey];
    
    NSArray *buildConfigurations = buildConfigurationList[@"buildConfigurations"];
    
    NSMutableArray *names = [NSMutableArray array];
    
    for (NSString *configurationKey in buildConfigurations) {
        NSDictionary *configuration = objects[configurationKey];
        [names addObject: configuration[@"name"]];
    }
    return names;
}

@interface NSString (SendTo)

- (void)sendToFileHandle:(NSFileHandle *)fileHandle;

- (void)sendToStdOut;

- (void)sendToStdErr;

@end

@implementation NSString (SendTo)

- (void)sendToFileHandle:(NSFileHandle *)fileHandle
{
    [fileHandle writeData: [self  dataUsingEncoding: NSUTF8StringEncoding]];
    [fileHandle writeData: [@"\n" dataUsingEncoding: NSUTF8StringEncoding]];
}

- (void)sendToStdOut
{
    [self sendToFileHandle: [NSFileHandle fileHandleWithStandardOutput]];
}

- (void)sendToStdErr
{
    [self sendToFileHandle: [NSFileHandle fileHandleWithStandardError]];
}

@end

int main(int argc, const char * argv[])
{
    int result = 0;
    
    @autoreleasepool {
        // insert code here...
        NSError *error = NULL;
        
        NSMutableSet *excludeList = [NSMutableSet set];
        
        NSArray *params = CmdLineArgumentsAsArray(argc, argv);
        
        __block NSString *xcodeproj = nil;
        
        [[params subarrayWithRange: NSMakeRange(1, params.count - 1)] enumerateObjectsUsingBlock: ^(NSString *param, NSUInteger idx, BOOL *stop) {
            //
            if ([param hasPrefix: @"-no"]) {
                [excludeList addObject: [param substringFromIndex: 3]];
            } else {
                xcodeproj = param;
            }
            *stop = xcodeproj != NULL;
        }];
        if (!xcodeproj) {
            NSString *errorMsg = [NSString stringWithFormat: @"Usage: %@ [-no<configname> -no<configname> ...] <xcode project file>", [params firstObject]];
            error = [NSError errorWithDomain: ConfiglistErrorDomain code: 1 userInfo: @{NSLocalizedDescriptionKey: errorMsg}];
        }
        NSPredicate *excludeConfigsPredicate = NULL;
        if (!error) {
            NSMutableArray *predicates = [NSMutableArray array];
            
            for (NSString *configName in excludeList) {
                [predicates addObject: [NSPredicate predicateWithFormat: @"SELF != [cd] %@", configName]];
            }
            excludeConfigsPredicate = [NSCompoundPredicate andPredicateWithSubpredicates: predicates];
        }
        NSData *data = NULL;
        if (!error) {
            xcodeproj = [[xcodeproj stringByDeletingPathExtension] stringByAppendingPathExtension: @"xcodeproj"];
            
            NSWorkspace *workspace = [NSWorkspace sharedWorkspace];
            if ([workspace isFilePackageAtPath: xcodeproj]) {
                NSString *fileName = [xcodeproj stringByAppendingPathComponent: @"project.pbxproj"];
                data = [NSData dataWithContentsOfFile: fileName options: kNilOptions error: &error];
            } else {
                NSString *errorMsg = [NSString stringWithFormat: @"File %@ is not package", xcodeproj];
                error = [NSError errorWithDomain: ConfiglistErrorDomain code: 2 userInfo: @{NSLocalizedDescriptionKey: errorMsg}];
            }
        }
        if (!error) {
            NSPropertyListFormat plistFormat;
            NSDictionary *project = [NSPropertyListSerialization propertyListWithData: data options: NSPropertyListImmutable format: &plistFormat error: &error];
            if (!error) {
                @try {
                    NSArray *names = [ListOfConfigs(project) filteredArrayUsingPredicate: excludeConfigsPredicate];
                    [names makeObjectsPerformSelector: @selector(sendToStdOut)];
                }
                @catch (NSException *exception) {
                    NSString *errorMsg = @"Unknown file format";
                    error = [NSError errorWithDomain: ConfiglistErrorDomain code: 3 userInfo: @{NSLocalizedDescriptionKey: errorMsg}];
                }
            }
        }
        if (error) {
            [error.localizedDescription sendToStdErr];
            result = (int)error.code;
        }
    }
    return result;
}
